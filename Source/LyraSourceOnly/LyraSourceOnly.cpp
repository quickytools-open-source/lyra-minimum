// Copyright Epic Games, Inc. All Rights Reserved.

#include "LyraSourceOnly.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LyraSourceOnly, "LyraSourceOnly" );
